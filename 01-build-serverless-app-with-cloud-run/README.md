# Lab: [Build a Serverless App with Cloud Run that Creates PDF Files][1]

## GSP644

![GSP644](../resources/gsp644.png)

## Overview

For the labs in the [Google Cloud Serverless Workshop: Pet Theory Quest][2], you will read through a fictitious business
scenario and assist the characters with their serverless migration plan.

Twelve years ago, Lily started the Pet Theory chain of veterinary clinics. Pet Theory currently sends invoices in DOCX
format to clients, but many clients have complained that they are unable to open them. To improve customer satisfaction,
Lily has asked Patrick in IT to investigate an alternative to improve the current situation.

Pet Theory's Ops team is a single person, so they are keen to invest in a cost efficient solution that doesn't require a
lot of ongoing maintenance. After analyzing the various processing options, Patrick decides to use [Cloud Run.][3]

Cloud Run is serverless, so it abstracts away all infrastructure management and lets you focus on building your
application instead of worrying about overhead. As a Google serverless product, it is able to scale to zero, meaning it
won't incur cost when not used. It also lets you use custom binary packages based on containers, which means building
consistent isolated artifacts is now feasible.

In this lab you will build a PDF converter web app on Cloud Run that automatically converts files stored in Cloud
Storage into PDFs stored in seperated folders.

### Architecture

This diagram gives you an overview of the services you will be using and how they connect to one another:

![architecture](../resources/01/architecture.png)

### What you will learn

In this lab, you will learn how to:

* Convert a Node JS application to a container.
* Build containers with Google Cloud Build.
* Create a Cloud Run service that converts files to PDF files in the cloud.
* Use event processing with Cloud Storage

### Prerequisites

This is a fundamental level lab. This assumes familiarity with the Cloud Console and shell environments. Experience with
Firebase will be helpful, but it is not required. Before taking this lab it is recommended that you have completed the
following Qwiklabs before taking this one:

* [Migrating Data to a Firestore Database][4]
* [Build a Serverless Web App with Firebase and Firestore][5]

You should also be comfortable editing files. You can use your favorite text editor (like nano, vi, etc.) or you can
launch the code editor from Cloud Shell, which can be found in the top ribbon:

Once you're ready, scroll down and follow the steps below to setup your lab environment.

## Setup and Requirements

## Understanding the task

Pet theory would like to convert their invoices into PDFs so that customers can open them reliably. The team wants to
accomplish this conversion automatically to minimize the workload for Lisa, the office manager.

Ruby, Pet Theory's computer consultant, gets a message from Patrick in IT...

"Hi Ruby,

I've done some research and found that [LibreOffice][6] is good at converting many different file formats to PDF.

Would it be possible to run LibreOffice in the cloud without having to maintain the servers?

Patrick"

"Hey Patrick,

I think I have just the thing for this type of situation.

I just watched a great video from Next 19 about [Cloud Run][7] on YouTube. It looks like we can run LibreOffice in a
serverless environment with Cloud Run. No server maintenance is needed!

I'll send over some resources that will help you get set up.

Ruby"

Help Patrick set up and deploy Cloud Run.

## Enable the Cloud Run API

## Deploy a simple Cloud Run Service

Ruby has developed a Cloud Run prototype and would like Patrick to deploy it onto Google Cloud. Now help Patrick
establish the PDF Cloud Run service for Pet Theory.

1. Open a new Cloud Shell session and run the following command to clone the Pet Theory repository:

    ```shell
    git clone https://github.com/rosera/pet-theory.git
    ```

2. Then change your current working directory to lab03:

    ```shell
    cd pet-theory/lab03
    ```

3. Edit package.json with Cloud Shell Code Editor or your preferred text editor. In the "scripts" section, add "
   start": "node index.js", as shown below:

    ```shell
    
    "scripts": {
        "start": "node index.js",
        "test": "echo \"Error: no test specified\" && exit 1"
      },
    
    ```

4. Now run the following commands in Cloud Shell to install the packages that your conversion script will be using:

    ```shell
    npm install express
    npm install body-parser
    npm install child_process
    npm install @google-cloud/storage
    ```

5. Now open the lab03/index.js file and review the code.

   The application will be deployed as a Cloud Run service that accepts HTTP POSTs. If the POST request is a Pub/Sub
   notification about an uploaded file, the service writes the file details to the log. If not, the service simply
   returns the string "OK".

6. Review the file named lab03/Dockerfile.

   The above file is called a manifest and provides a recipe for the Docker command to build an image. Each line begins
   with a command that tells Docker how to process the following information:

    * The first list indicates the base image should use node v12 as the template for the image to be created.

    * The last line indicates the command to be performed, which in this instance refers to "npm start".

7. To build and deploy the REST API, use Google Cloud Build. Run this command to start the build process:

   ```shell
   gcloud builds submit \
     --tag gcr.io/$GOOGLE_CLOUD_PROJECT/pdf-converter
   ```

   The command builds a container with your code and puts it in the Container Registry of your project.

8. Return to the Cloud Console, open the navigation menu, and select Container Registry > Images. You should see your
   container hosted:

   ![container](../resources/01/container.png)

9. Return to your code editor tab and in Cloud Shell run the following command to deploy your application:

   ```
   student_00_e2e448058e04@cloudshell:~/pet-theory/lab03 (qwiklabs-gcp-01-2078bc5213f4)$ gcloud beta run deploy pdf-converter \
   >   --image gcr.io/$GOOGLE_CLOUD_PROJECT/pdf-converter \
   >   --platform managed \
   >   --region us-central1 \
   >   --no-allow-unauthenticated
   Deploying container to Cloud Run service [pdf-converter] in project [qwiklabs-gcp-01-2078bc5213f4] region [us-central1]
   ✓ Deploying new service... Done.                                                           
   ✓ Creating Revision...
   ✓ Routing traffic...
   Done.
   Service [pdf-converter] revision [pdf-converter-00001-kol] has been deployed and is serving 100 percent of traffic.
   Service URL: https://pdf-converter-ryh2hongqa-uc.a.run.app
   ``` 

10. When the deployment is complete, you will see a message like this:

```shell
Service [pdf-converter] revision [pdf-converter-00001] has been deployed and is serving 100 percent of traffic
at https://pdf-converter-[hash].a.run.app
``` 

11. Create the environment variable $SERVICE_URL for the app so you can easily access it:

```shell
SERVICE_URL=$(gcloud beta run services describe pdf-converter --platform managed --region us-central1 --format="value(status.url)")
echo $SERVICE_URL
```

12. Make an anonymous POST request to your new service:

```shell
curl -X POST $SERVICE_URL
``` 

This will result in an error message saying "Your client does not have permission to get the URL". This is good; you
don't want the service to be callable by anonymous users.

13. Now try invoking the service as an authorized user:

```shell
curl -X POST -H "Authorization: Bearer $(gcloud auth print-identity-token)" $SERVICE_URL 
```
If you get the response "OK", you have successfully deployed a Cloud Run service. Well done!

## Trigger your Cloud Run service when a new file is uploaded

Now that the Cloud Run service has been successfully deployed, Ruby would like Patrick to create a staging area for the
data to be converted. The Cloud Storage bucket will use an event trigger to notify the application when a file has been
uploaded and needs to be processed.

1. Run the following command to create a bucket in Cloud Storage for the uploaded docs:

   ```shell
   gsutil mb gs://$GOOGLE_CLOUD_PROJECT-upload
   ```

2. And another bucket for the processed PDFs:

   ```shell
   gsutil mb gs://$GOOGLE_CLOUD_PROJECT-processed
   ``` 

3. Now return to your Cloud Console tab, open the Navigation menu and select Storage. Verify that the buckets have been
   created (there will be other buckets there as well that are used by the platform.)


4. In Cloud Shell run the following command to tell Cloud Storage to send a Pub/Sub notification whenever a new file has
   finished uploading to the docs bucket:

   ```shell
   gsutil notification create -t new-doc -f json -e OBJECT_FINALIZE gs://$GOOGLE_CLOUD_PROJECT-upload 
   ```

   The notifications will be labeled with the topic "new-doc".

5. Then create a new service account which Pub/Sub will use to trigger the Cloud Run services:
   ````shell
   gcloud iam service-accounts create pubsub-cloud-run-invoker --display-name "PubSub Cloud Run Invoker"
   ````

6. Give the new service account permission to invoke the PDF converter service:

   ````shell
   gcloud beta run services add-iam-policy-binding pdf-converter --member=serviceAccount:
   pubsub-cloud-run-invoker@$GOOGLE_CLOUD_PROJECT.iam.gserviceaccount.com --role=roles/run.invoker --platform managed
   --region us-central1 
   ````

7. Find your project number by running this command:

   ````shell
   gcloud projects list
   ````

   Look for the project whose name starts with "qwiklabs-gcp-". You will be using the value of the Project Number in the
   next command.

8. Create a `PROJECT_NUMBER` environment variable, replacing [project number] with the Project Number from the last
   command:

   ````shell
   PROJECT_NUMBER=[project number]
   ````

9. Then enable your project to create Cloud Pub/Sub authentication tokens:

   ````shell
   gcloud projects add-iam-policy-binding $GOOGLE_CLOUD_PROJECT --member=serviceAccount:
   service-$PROJECT_NUMBER@gcp-sa-pubsub.iam.gserviceaccount.com --role=roles/iam.serviceAccountTokenCreator
   ````

10. Finally, create a Pub/Sub subscription so that the PDF converter can run whenever a message is published on the topic "
new-doc".

   ````shell
   gcloud beta pubsub subscriptions create pdf-conv-sub --topic new-doc --push-endpoint=$SERVICE_URL
   --push-auth-service-account=pubsub-cloud-run-invoker@$GOOGLE_CLOUD_PROJECT.iam.gserviceaccount.com
   ````


## See if the Cloud Run service is triggered when files are uploaded to Cloud Storage

To verify the application is working as expected, Ruby asks Patrick to upload some test data to the named storage bucket
and then check Cloud Logging.

1. Copy some test files into your upload bucket:

   ```shell
   gsutil -m cp gs://spls/gsp644/* gs://$GOOGLE_CLOUD_PROJECT-upload
   ```

2. Once the upload is done, return to your Cloud Console tab, open the navigation menu, and select Logging from under
   the Operations section.

3. In the first dropdown, filter your results to Cloud Run Revision and click Add. Then click Run Query.

4. In the Query results, look for a log entry that starts with file: and click it. It shows a dump of the file data that
   Pub/Sub sends to your Cloud Run service when a new file is uploaded.

5. Can you find the name of the file you uploaded in this object?

   ![log](../resources/01/log.png)

6. Now return to the code editor tab and run the following command in Cloud Shell to clean up your upload directory by
   deleting the files in it:

   ```shell
   gsutil -m rm gs://$GOOGLE_CLOUD_PROJECT-upload/*
   ```

## Docker containers

## Testing the pdf-conversion service

## Congratulations!

## End your lab

[1]: https://run.qwiklabs.com/focuses/8390?parent=catalog

[2]: https://google.qwiklabs.com/quests/98

[3]: https://cloud.google.com/run/

[4]: https://google.qwiklabs.com/focuses/8392?parent=catalog

[5]: https://google.qwiklabs.com/focuses/8391?parent=catalog

[6]: https://www.libreoffice.org/

[7]: https://www.youtube.com/watch?v=16vANkKxoAU&t=1317s